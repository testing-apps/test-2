# Test App 2

This repo has the following bugs:
- Login as admin > Employees > Edit Employee > Make Admin > Edit same employee > Remove Admin > 404 red screen
- In Navbar at top, next to Settings, it should say User's name > it always says 'Admin'
- Login as employee > Notes > Delete any note > Note doesn't destroy
- Log out > In address bar, add '/teams/1' to end of URL > Page should only be visible if logged in
- Log in as employee > Settings > My History > always goes to history of first employee

# Seeding

```ruby
rake db:create 
rake db:seed 
rake teams:seed_team
```