# frozen_string_literal: true

class CreateNoteRoadblocks < ActiveRecord::Migration[5.2]
  def change
    create_table :note_roadblocks do |t|
      t.text :body
      t.references :note, foreign_key: true, on_delete: :nullify
      t.references :employees, foreign_key: true

      t.timestamps
    end
  end
end
