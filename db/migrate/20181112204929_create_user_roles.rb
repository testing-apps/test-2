# frozen_string_literal: true

class CreateUserRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :user_roles do |t|
      t.references :users, foreign_key: true
      t.integer :role

      t.timestamps
    end
  end
end
