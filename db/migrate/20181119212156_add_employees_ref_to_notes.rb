# frozen_string_literal: true

class AddEmployeesRefToNotes < ActiveRecord::Migration[5.2]
  def change
    add_reference :notes, :employees, foreign_key: true
  end
end
