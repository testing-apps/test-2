# frozen_string_literal: true

class CreateNoteBodies < ActiveRecord::Migration[5.2]
  def change
    create_table :note_bodies do |t|
      t.text :body
      t.references :note, foreign_key: true, on_delete: :cascade
      t.references :employees, foreign_key: true

      t.timestamps
    end
  end
end
