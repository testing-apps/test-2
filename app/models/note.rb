# frozen_string_literal: true

class Note < ApplicationRecord
  has_many :comments, dependent: :delete_all
  has_many :note_bodies, dependent: :delete_all
  has_many :note_roadblocks, dependent: :nullify

  accepts_nested_attributes_for :note_bodies, :note_roadblocks

  after_find :ensure_teams_id_has_a_value

  validates :title, presence: true, length: { minimum: 1 }
  validates :teams_id, presence: true

  default_scope { order(created_at: :desc, id: :desc) }

  def team
    Team.find(teams_id)
  end

  def author
    Employee.find(employees_id)
  end

  def bodies
    NoteBody.where(note_id: id)
  end

  def body_for(employee_id)
    NoteBody.find_by(note_id: id, employees_id: employee_id)&.body
  end

  def roadblock_for(employee_id)
    NoteRoadblock.find_by(id: id, employees_id: employee_id)&.body
  end

  def roadblocks
    NoteRoadblock.where(note_id: id)
  end

  private

  def ensure_teams_id_has_a_value
    return unless teams_id.nil?

    self.teams_id = Team::SnapIT.id
    save
  end
end
