# frozen_string_literal: true

class NoteBody < ApplicationRecord
  belongs_to :note

  def employee
    Employee.find(employees_id)
  end
end
