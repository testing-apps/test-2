# frozen_string_literal: true

class NoteRoadblock < ApplicationRecord
  has_many :solutions, dependent: :delete_all
  belongs_to :note
end
