// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on("turbolinks:load", function() {
  $("#team-members-assign").hide();
  var teamBoxes = $(".team-member-boxes");

  $("#lead-select").change(function() {
    $("#team-members-assign").show();
    $.each(teamBoxes, function(index, value) {
      if ($(this).attr("id") == $("#lead-select").val()) {
        $(this).hide();
      } else {
        $(this).show();
      }
    });
  });
});
