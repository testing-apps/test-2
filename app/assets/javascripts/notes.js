$(document).on("turbolinks:load", function() {
  var teams = $(".teams");
  var employee = $(".employee");

  // Create an array of all employees.
  var allEmployees = [];
  $.each(employee, function() {
    allEmployees.push($(this).attr("id"));
  });

  // When the page first loads, hide all employees.
  $.each(allEmployees, function() {
    $(`#${this}`).hide();
  });

  // Display employees for Teams that are selected (checked:true).
  // List of Employees to display is built upon each click of checkbox.
  $.each(teams, function() {
    $(this).click(function() {

      // Create an array of employees whose Team is selected.
      var selected = []
      $('input:checked').each(function() {
        value = $(this).data("team");
        selected = selected.concat(value);
      })

      // If employee ID is included in selected Teams, display Employee.
      $.each(allEmployees, function(_, employee) {

        // Employee ID number has to be converted to integer data type.
        employeeIdNumber = parseInt(employee, 10);

        if (selected.includes(employeeIdNumber)){
          $(`#${employee}`).show();
        } else {
          $(`#${employee}`).hide();
        }
      });
    });
  });
});
