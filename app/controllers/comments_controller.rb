# frozen_string_literal: true

class CommentsController < ApplicationController
  before_action :employee?
  before_action :authorize

  def create
    @note = Note.find(params[:note_id])

    if params[:comment][:body].blank?
      redirect_to note_path(@note), danger: 'Comment cannot be empty'
      return
    end

    @comment = @note.comments.create(
      body: params[:comment][:body],
      note_id: params[:note_id],
      users_id: current_user.id
    )

    redirect_to note_path(@note)
  end

  def destroy
    note = Note.find(params[:note_id])
    comment = note.comments.find(params[:id])
    comment.destroy
    redirect_to note_path(note)
  end
end
