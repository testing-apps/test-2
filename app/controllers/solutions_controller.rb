# frozen_string_literal: true

class SolutionsController < ApplicationController
  before_action :authorize
  before_action :employee?, only: %i[create destroy]

  def create
    note_roadblock = NoteRoadblock.find(params[:note_roadblock_id])
    note_roadblock.solutions.create(
      body: params[:solution][:body],
      note_roadblock_id: params[:note_roadblock_id],
      users_id: current_user.id
    )
    redirect_to note_roadblock_path(params[:note_roadblock_id])
  end

  def destroy
    Solution.find(params[:id]).destroy
    redirect_to note_roadblock_path(params[:note_roadblock_id])
  end

  def show
    @roadblock = NoteRoadblock.find(params[:id])
    @roadblock_owner = Employee.find(@roadblock.employees_id)
    @solutions = Solution.where(note_roadblock_id: params[:id])
    @employees = Employee.all
    @users = User.all
  end
end
