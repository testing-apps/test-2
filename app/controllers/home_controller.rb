# frozen_string_literal: true

class HomeController < ApplicationController
  require 'date'
  before_action :authorize

  def index
    @employees = Employee.active
    @user = params[:user]

    day = DateTime.now
    @week = [day]
    loop do
      day = day.prev_day
      @week << day unless day.cwday > 5
      break if @week.length == 5
    end

    @notes = Note.where(created_at: @week.last.beginning_of_day..@week.first.end_of_day)
  end
end
