# frozen_string_literal: true

class EmployeesController < ApplicationController
  before_action :authorize

  def show
    @employee = Employee.find(params[:id])
    @teams = @employee.teams
    @user = @employee.as_user

    note_bodies = NoteBody.where(employees_id: params[:id]).map(&:note_id)
    note_roadblocks = NoteRoadblock.where(employees_id: params[:id]).map(&:note_id)
    all_notes = note_bodies | note_roadblocks

    all_notes_for_employee = Note.find(all_notes.compact)

    @employee_notes = []
    all_notes_for_employee&.each do |note|
      @employee_notes << {
        date: note.created_at,
        id: note.id,
        title: note.title,
        body: note.body_for(@employee.id),
        roadblocks: note.roadblock_for(@employee.id)
      }
    end

    @records = []

    EmployeesTeam.where(employees_id: @employee.id).each do |record|
      @records << "Added to #{Team.find(record.teams_id).name} on #{record.created_at.in_time_zone('America/Chicago').strftime('%b %d, %Y')}" # rubocop:disable Metrics/LineLength
      unless record.active?
        @records << "Removed from #{Team.find(record.teams_id).name}  on #{record.updated_at.in_time_zone('America/Chicago').strftime('%b %d, %Y')}" # rubocop:disable Metrics/LineLength
      end
    end
  end

  def destroy
    employee = Employee.find(params[:id])
    user = employee.as_user

    if Team.leads.include?(employee)
      redirect_to admin_index_path, danger: "Unable to remove #{employee.first_name} while still a pod lead. Please remove from role and try again." # rubocop:disable Metrics/LineLength
    else
      is_success = ActiveRecord::Base.transaction do
        raise ActiveRecord::Rollback unless EmployeesTeam.where(employees_id: employee.id).update_all(active: false)
        raise ActiveRecord::Rollback unless UserRole.where(users_id: user.id).destroy_all

        employee.update!(users_id: nil)
        user.destroy!
      end
      if is_success
        redirect_to admin_index_path, success: 'Employee deleted'
      else
        redirect_to admin_index_path, danger: 'Unable to remove employee'
      end
    end
  end

  def authorize_access_to_employee_info
    redirect_to home_path unless current_user.admin? || params[:id].to_i == current_user.as_employee&.id
  end
end
