# frozen_string_literal: true

class PasswordController < ApplicationController
  before_action :authorize

  def edit; end

  def update
    user = User.find(session[:user_id])
    passwords = params[:password]

    if !user&.authenticate(passwords[:old])
      redirect_to '/password', danger: 'Old Password is invalid'
    elsif /[^a-zA-Z0-9]/.match?(passwords[:new].to_s)
      redirect_to '/password', danger: 'Invalid characters in new password'
    elsif (passwords[:confirm_new]) != (passwords[:new])
      redirect_to '/password', danger: 'Confirm does not match'
    else
      user.password = (passwords[:new])
      session[:user_id] = nil
      user.save
      redirect_to '/login', success: 'Password Updated!'
    end
  end
end
